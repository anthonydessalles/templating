var gulp = require('gulp');
var twig = require('gulp-twig');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');

var bootstrap_css = 'node_modules/bootstrap/dist/css/bootstrap.css';
var bootstrap_js = 'node_modules/bootstrap/dist/js/bootstrap.js';
var font_awesome_css = 'node_modules/font-awesome/css/font-awesome.min.css';
var font_awesome_fonts = 'node_modules/font-awesome/fonts/fontawesome-webfont.*';
var jquery = 'node_modules/jquery/dist/jquery.min.js';

// Library
var library = {
    css: [
        bootstrap_css,
        font_awesome_css,
        'library-2018/source/custom_library.css'
    ],
    js: [
        jquery,
        bootstrap_js
    ],
    source: 'library-2018/source/*.twig',
    destination: 'library-2018/templates'
};

gulp.task('library_css', function () {
    return gulp
        .src(library.css)
        .pipe(cleanCSS())
        .pipe(concat('library.css'))
        .pipe(gulp.dest(library.destination + "/css"));
});

gulp.task('library_js', function() {
    return gulp
        .src(library.js)
        .pipe(uglify())
        .pipe(concat("library.js"))
        .pipe(gulp.dest(library.destination + "/js"));
});

gulp.task('library_fonts', function() {
    return gulp
        .src(font_awesome_fonts)
        .pipe(gulp.dest("font-awesome/fonts"));
});

gulp.task('library_twig', function() {
    return gulp.src(library.source)
        .pipe(twig())
        .pipe(gulp.dest(library.destination));
});

gulp.task('library', [
    'library_css',
    'library_js',
    'library_fonts',
    'library_twig'
]);

// Games
var games = {
    css: [
        bootstrap_css,
        'games-2018/source/css/lightbox.min.css',
        'games-2018/source/css/lity.min.css',
        'games-2018/source/css/style.css'
    ],
    js: [
        jquery,
        bootstrap_js,
        'games-2018/source/js/lightbox.min.js',
        'games-2018/source/js/lity.min.js'
    ],
    source: 'games-2018/source/_layouts/default.html',
    destination: 'games-2018/templates'
};

gulp.task('games_css', function () {
    return gulp
        .src(games.css)
        .pipe(cleanCSS())
        .pipe(concat('games.css'))
        .pipe(gulp.dest(games.destination + "/css"));
});

gulp.task('games_js', function() {
    return gulp
        .src(games.js)
        .pipe(uglify())
        .pipe(concat("games.js"))
        .pipe(gulp.dest(games.destination + "/js"));
});

gulp.task('games_twig', function() {
    return gulp.src(games.source)
        .pipe(twig({
            data: {
                page: {
                    title: "Home"
                },
                site: {
                    title: "Games",
                    url: "file:///home/adessalles/work/templating/games-2018/templates/"
                },
            }
        }))
        .pipe(gulp.dest(games.destination));
});

gulp.task('games', [
    'games_css',
    'games_js',
    'games_twig'
]);

// Default
gulp.task('default', [
    'library',
    'games'
]);
